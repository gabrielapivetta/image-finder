$(document).ready(function(){
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
});

$('#confirmTokenDeletionModal').on('show.bs.modal', function (event) {
    let tokenId = $(event.relatedTarget).data('token-id')
    $(this).find('.modal-footer a')
           .attr("href", "/service_tokens/delete/".concat(tokenId))
})

function copyClipboard(id) {
    var token = document.getElementById(id);
    token.select();
    document.execCommand('copy')
}