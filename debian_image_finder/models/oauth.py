from debian_image_finder.extensions.database import db
from flask_dance.consumer.storage.sqla import OAuthConsumerMixin
from debian_image_finder.models.user import User


class OAuth(OAuthConsumerMixin, db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)
