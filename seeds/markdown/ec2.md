#### Official Debian Amazon Machine Images (AMIs)
- [Debian AMIs](http://deb.li/awsmp) on AWS [Marketplace](https://wiki.debian.org/Cloud/AmazonEC2Image/Marketplace) (official Debian account 379101102735)

- [Debian Stretch AMIs 9.x.x](https://wiki.debian.org/Cloud/AmazonEC2Image/Stretch)

#### Historical releases of Debian for Amazon EC2
- [Debian Jessie AMIs 8.x.x](https://wiki.debian.org/Cloud/AmazonEC2Image/Jessie)

- [Debian Wheezy AMIs 7.x](https://wiki.debian.org/Cloud/AmazonEC2Image/Wheezy)

- [Debian Squeeze AMIs 6.0.x](https://wiki.debian.org/Cloud/AmazonEC2Image/Squeeze)

#### Further information
For discussion about Debian on various cloud providers, please visit [Debian-Cloud mailing list](http://lists.debian.org/debian-cloud/).

Please report bugs to the `cloud.debian.org` pseudo-package, when they are related to the choices made when building the image (which packages to include, which customisation was made, etc.). Advanced users can add the [usertags](https://udd.debian.org/cgi-bin/bts-usertags.cgi?user=cloud.debian.org%40packages.debian.org) to triage the report more precisely.

See also our [EC2 FAQ](https://wiki.debian.org/Amazon/EC2/FAQ).

AMI builds
Starting with the stretch (9.x) release, the cloud team has been using the [FAI](http://fai-project.org/) tool to generate the AMIs. The FAI configuration used for the stretch EC2 images can be found in the [debian/stretch](https://salsa.debian.org/cloud-team/debian-cloud-images/tree/debian/stretch) branch of the [FAI Cloud Images](https://salsa.debian.org/cloud-team/debian-cloud-images) repository on Salsa. There's a brief overview describing how to generate your own (possibly customized) AMIs based on these configs on [Noah's blog](https://noah.meyerhans.us/blog/2017/02/10/using-fai-to-customize-and-build-your-own-cloud-images/).

#### Individual Developer/User Contributed AMIs

### <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>

Security of user-contributed AMIs.

Please consider the security of user-contributed AMIs as having been potentially untested and un-vetted by the Debian Project. Always check the AMI ID matches what you were looking for, and verify the AMI Source to confirm the AMI you are launch is being updated and controlled from an account ID that you trust.

Questions about the images published by [tom@punch.net](tom@punch.net), visit the [ec2debian google group](http://groups.google.com/group/ec2debian). For more information on images published by RightScale, see the [RightScale OSS](http://support.rightscale.com/21-Community/RightScale_OSS#) support page.

#### Debian installer
See [Cloud/AmazonEC2DebianInstaller](https://wiki.debian.org/Cloud/AmazonEC2DebianInstaller).