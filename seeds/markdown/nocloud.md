Mostly useful for testing the build process itself.
It doesn't have cloud-init installed but instead allows root login without a password.