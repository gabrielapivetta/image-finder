FROM debian:unstable-slim

LABEL Cloud Team "debian-cloud@lists.debian.org"

EXPOSE 5000

WORKDIR /debian-image-finder

ARG DEBIAN_FRONTEND=noninteractive
ARG DEBCONF_NOWARNINGS="yes"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get upgrade -y && \
    apt-get install --no-install-recommends -y \
    gunicorn \
    python3-blinker \
    python3-coverage \
    python3-dev \
    python3-dotenv \
    python3-dynaconf \
    python3-flasgger \
    python3-flask \
    python3-flask-login \
    python3-flask-marshmallow \
    python3-flask-migrate \
    python3-flask-restful \
    # python3-flask-seeder -> needs to move from experimental to unstable
    python3-flaskext.wtf \
    python3-flask-script \
    python3-flask-sqlalchemy \
    python3-flask-testing \
    python3-flake8 \
    python3-jwt \
    python3-markdown2 \
    python3-marshmallow-sqlalchemy \
    python3-nose2 \
    python3-pip \
    python3-psycopg2 \
    python3-requests \
    python3-setuptools \
    python3-werkzeug \
    libjs-bootstrap4 \
    libpq-dev \
    libjs-jquery \
    libjs-jquery-datatables \
    fonts-font-awesome && \
    pip3 --no-cache-dir install Flask-Seeder Flask-SimpleMDE flask_dance && \
    apt-get -q -y clean && \
    rm -rf /var/lib/apt/lists/*

COPY . /debian-image-finder

ENTRYPOINT ["./docker-entrypoint.sh"]
