#!/bin/sh

set -e

flask db upgrade

if [ "$FLASK_ENV" = "production" ]; then
    gunicorn -c gunicorn.config.py "debian_image_finder:create_app()"
else
    flask run --host 0.0.0.0
fi